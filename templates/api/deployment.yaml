apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "vikunja.apiFullname" . }}
  labels:
    {{- include "vikunja.labels" . | nindent 4 }}
spec:
  {{- if not .Values.api.autoscaling.enabled }}
  replicas: {{ .Values.api.replicaCount }}
  {{- end }}
  {{- with .Values.api.extraDeploymentSpec }}
  {{- toYaml . | nindent 2 }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "vikunja.apiSelectorLabels" . | nindent 6 }}
  template:
    metadata:
      annotations:
        checksum/config: {{ include (print $.Template.BasePath "/secret.yaml") . | sha256sum }}
      {{- with .Values.api.podAnnotations }}
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "vikunja.apiSelectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.api.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "vikunja.serviceAccountName" . }}
      {{- with .Values.api.podSecurityContext }}
      securityContext:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.api.initContainers }}
      initContainers:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      containers:
      {{- with .Values.api.sidecarContainers }}
      {{- toYaml . | nindent 8 }}
      {{- end }}
      - name: api
        {{- with .Values.api.securityContext }}
        securityContext:
          {{- toYaml . | nindent 10 }}
        {{- end }}
        {{- $image := required ".image.repository is not set" .Values.api.image.repository }}
        {{- $tag := default .Chart.AppVersion .Values.api.image.tag | toString }}
        image: {{ printf "%s:%s" $image $tag | quote }}
        imagePullPolicy: {{ .Values.api.image.pullPolicy }}
        ports:
        - name: http
          containerPort: {{ (.Values.api.config.service.interface | split ":")._1 | int }}
          protocol: TCP
        {{- with .Values.api.env }}
        env:
        {{- toYaml . | nindent 8 }}
        {{- end }}
        {{- with .Values.api.envFrom }}
        envFrom:
        {{- toYaml . | nindent 8 }}
        {{- end }}
        {{- with .Values.api.livenessProbe }}
        livenessProbe:
          {{- toYaml . | nindent 10 }}
        {{- end }}
        {{- with .Values.api.readinessProbe }}
        readinessProbe:
          {{- toYaml . | nindent 10 }}
        {{- end }}
        {{- with .Values.api.startupProbe }}
        startupProbe:
          {{- toYaml . | nindent 10 }}
        {{- end }}
        {{- with .Values.api.resources }}
        resources:
          {{- toYaml . | nindent 10 }}
        {{- end }}
        volumeMounts:
        - name: tmp
          mountPath: /tmp
        - name: api-config
          subPath: api-config.yaml
          mountPath: /etc/vikunja/config.yaml
        {{- if .Values.api.persistence.enabled }}
        - name: storage
          subPath: files
          mountPath: {{ quote .Values.api.config.files.basepath }}
        {{- if eq .Values.api.config.database.type "sqlite" }}
        - name: storage
          subPath: sqlite
          mountPath: {{ dir .Values.api.config.database.path | quote }}
        {{- end }}
        {{- end }}
        {{- with .Values.api.extraVolumeMounts }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
      volumes:
      - name: tmp
        emptyDir: {}
      - name: api-config
        secret:
          secretName: {{ include "vikunja.fullname" . }}
          defaultMode: 0644
      {{- if .Values.api.persistence.enabled }}
      - name: storage
        persistentVolumeClaim:
          claimName: {{ include "vikunja.apiFullname" . }}
      {{- end }}
      {{- with .Values.api.extraVolumes }}
      {{- toYaml . | nindent 6 }}
      {{- end }}
      {{- with .Values.api.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.api.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.api.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
